import 'package:flutter/cupertino.dart';

abstract class AOpenPageAction {

  OpenPageAction openBalance({Widget? widget,String name = ""}) {
    return OpenPageAction(
      key: "open_page_balance",
    );
  }

  OpenPageAction openPromoCode({Widget? widget,String name = ""}) {
    return OpenPageAction(
      key: "open_promo_code",
    );
  }

  OpenPageAction openSettings({Widget? widget,String name = ""}) {
    return OpenPageAction(
      key: "open_settings",
    );
  }

  OpenPageAction openServiceAndCountries({Widget? widget,String name = ""}) {
    return OpenPageAction(
      key: "open_service_and_countries",
    );
  }
}

class OpenPageActions extends AOpenPageAction {
  OpenPageActions();

  @override
  OpenPageAction openBalance({Widget? widget,String name = ""}) {
    return super.openBalance().copyWith(pageValue:widget,nameValue:name);
  }

  @override
  OpenPageAction openPromoCode({Widget? widget,String name = ""}) {
    return super.openPromoCode().copyWith(pageValue:widget,nameValue:name);
  }

  @override
  OpenPageAction openSettings({Widget? widget,String name = ""}) {
    return super.openSettings().copyWith(pageValue:widget,nameValue:name);
  }

  @override
  OpenPageAction openServiceAndCountries({Widget? widget,String name = ""}) {
    return super.openServiceAndCountries().copyWith(pageValue:widget,nameValue:name);
  }
}

class OpenPageAction {
  final String key;
  final String name;
  final Widget? page;

  OpenPageAction({
    required this.key,
    this.name = "",
    this.page,
  });

  OpenPageAction copyWith({String nameValue = "",Widget? pageValue}) {
    return OpenPageAction(key: key, page: pageValue,name: nameValue);
  }
}
