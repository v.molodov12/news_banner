abstract class Actions {
  ButtonAction notAction([String name = ""]) {
    return ButtonAction(
      key: "not_action",
    );
  }

  ButtonAction openPage([String name = ""]) {
    return ButtonAction(
      key: "open_page",
    );
  }

  ButtonAction openUrl([String name = ""]) {
    return ButtonAction(
      key: "open_url",
    );
  }

  Map<String, ButtonAction> getMapButtonActions();
}

class ButtonActions extends Actions {
  ButtonActions();

  @override
  ButtonAction notAction([String name = ""]) {
    return super.notAction().copyWith(name);
  }

  @override
  ButtonAction openPage([String name = ""]) {
    return super.openPage().copyWith(name);
  }

  @override
  ButtonAction openUrl([String name = ""]) {
    return super.openUrl().copyWith(name);
  }
  @override
  Map<String, ButtonAction> getMapButtonActions() {
    final notActionInstance = notAction();
    final openPageInstance = openPage();
    final openUrlInstance = openUrl();

    return {
      notActionInstance.key: notActionInstance,
      openPageInstance.key: openPageInstance,
      openUrlInstance.key: openUrlInstance,
    };
  }
}

class ButtonAction {
  final String key;
  final String name;

  ButtonAction({
    required this.key,
    this.name = "",
  });

  ButtonAction copyWith(String? nameValue) {
    return ButtonAction(key: key, name: nameValue ?? "");
  }
}
