import 'package:carousel_slider/carousel_controller.dart';
import 'package:flutter/widgets.dart';

class CarouselDecorator {
  ///[boxDecoration]A class for customizing the appearance of the main container.
  ///[boxDecoration]Класс для настройки внешнего вида основного контейнера.
  final Decoration? boxDecoration;
  ///[backgroundDecoration]A class for customizing the appearance of the background.
  ///[backgroundDecoration]Класс для настройки внешнего вида заднего фона.
  final Decoration? backgroundDecoration;
  ///If [widget] passed, it fills the entire space of the carousel card.
  ///Если [widget] не пустой, то он заполняет все пространство карточки карусели.
  final Widget? widget;
  ///[autoPlayInterval]The time after which the flipping occurs.Defult seconds: 15.
  ///[autoPlayInterval]Время через которое происходит перелистывание.Поумолчанию секунд: 15.
  final Duration autoPlayInterval;
  ///[autoPlay]if the true triggers scrolling.
  ///[autoPlay]Если истина запускает пролистывание.
  final bool autoPlay;
  ///[height]actual height of the carousel.
  ///[height]фактическая высота карусели.
  final double height;
  ///[initialPage]The tab from which the display starts.
  ///[initialPage]Вкладка, с которой начинается отображение.
  final int initialPage;
  ///[onPageChanged]Callback function that is triggered when the scrolling state changes.
  ///[onPageChanged]Функция обратного вызова, которая запускается при изменении состояния прокрутки.
  final ValueChanged<int>? onPageChanged;
  ///[carouselController]Controller for controlling the scrolling of the carousel.
  ///[carouselController]Контроллер для управления прокруткой карусели.
  final CarouselController? carouselController;

  const CarouselDecorator({
    this.widget,
    this.boxDecoration,
    this.backgroundDecoration,
    this.autoPlayInterval = const Duration(seconds: 15),
    this.autoPlay = true,
    this.height = 140,
    this.initialPage = 0,
    this.onPageChanged,
    this.carouselController,
  });
}
