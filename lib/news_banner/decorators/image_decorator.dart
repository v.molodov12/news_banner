import 'package:flutter/cupertino.dart';

class ImageDecorator {
  final String urlImages;
  final String pathToTheImages;
  final Color? colorPlaceholder;
  final BorderRadiusGeometry? clipRectRadius;
  final bool svgImages;
  final Widget? widget;
  final Widget? errorLoadImages;
  final double? width;
  final double? height;

  const ImageDecorator({
    this.width,
    this.widget,
    this.svgImages = false,
    this.height,
    this.clipRectRadius,
    this.colorPlaceholder,
    this.errorLoadImages,
    this.pathToTheImages = "",
    this.urlImages = "",
  });
}
