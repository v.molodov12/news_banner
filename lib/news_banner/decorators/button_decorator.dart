import 'package:flutter/cupertino.dart';

class ButtonDecorator {
  /// The [text] and [textStyle] determine the display of the text on the button.
  ///[text] и [textStyle] определяют отображение текста на кнопке.
  final String text;
  final TextStyle? textStyle;
  ///[decorationButton]A class for changing the appearance of a button style.
  ///[decorationButton]Класс для изменения внешнего вида виджета кнопки.
  final Decoration? decorationButton;
  ///[borderRadiusElevation]A class for controlling the rounding of the shadow falling when pressed.
  ///[borderRadiusElevation]Класс для управления закруглением тени, падающей при нажатии.
  final BorderRadius? borderRadiusElevation;
  ///[constraints]A class that specifies the minimum and maximum dimensions of the button.
  ///[constraints]Класс задающий минимальные и максимальные размеры кнопки.
  final BoxConstraints? constraints;
  ///if [widget] is passed, then fill all available space with it.
  ///Если передан [widget] то заполнить им все доступное пространство.
  final Widget? widget;
  /// The [width] and [height] of the widget, sets the forced dimensions.
  ///[width] and [height] высота и ширина виджета, задает принудительные размеры.
  final double? width;
  final double? height;

  const ButtonDecorator({
    this.text = "",
    this.textStyle,
    this.decorationButton,
    this.borderRadiusElevation,
    this.constraints,
    this.width,
    this.height,
    this.widget,
  });
}
