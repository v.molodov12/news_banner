import 'package:flutter/material.dart';
import 'package:news_banner/news_banner/decorators/button_decorator.dart';
import 'package:news_banner/news_banner/decorators/image_decorator.dart';
import 'package:news_banner/news_banner/widgets/button_banner.dart';
import 'package:news_banner/news_banner/widgets/images_banner.dart';

class NewsItem extends StatelessWidget {
  ///[text]The text displayed in the banner.
  ///[text]Текст отображаемый в баннере.
  final Widget? text;
  ///[description]The description displayed in the banner.
  ///[description]Описание отображаемый в баннере.
  final Widget? description;
  ///[buttonDecorator]A class for changing the appearance of a button.
  ///[buttonDecorator]Класс для изменения внешнего вида кнопки.
  final ButtonDecorator? buttonDecorator;
  ///[imageDecorator]A class for changing the appearance of a images.
  ///[imageDecorator]Класс для изменения внешнего вида картинки.
  final ImageDecorator? imageDecorator;
  ///[buttonClick]Callback of pressing the button.
  ///[buttonClick]обратный вызов при нажатии кнопки.
  final ValueChanged<bool> buttonClick;

  final EdgeInsetsGeometry? paddingImages;
  final EdgeInsetsGeometry? paddingButton;
  final EdgeInsetsGeometry? paddingText;
  final EdgeInsetsGeometry? padding;

  const NewsItem({
    Key? key,
    this.text,
    this.description,
    required this.buttonClick,
    this.buttonDecorator,
    this.imageDecorator,
    this.paddingImages,
    this.paddingButton,
    this.paddingText,
    this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Padding(
            padding: padding ?? const EdgeInsets.all(0.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child:Padding(
                        padding: paddingText?? const EdgeInsets.all(0.0),
                        child: text ??
                            const Text(
                              "text text text text text",
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                              ),
                              textAlign: TextAlign.left,
                            ),
                      )
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: paddingText?? const EdgeInsets.all(0.0),
                        child: description ??
                            const Text(
                              "description description description description",
                              overflow: TextOverflow.ellipsis,
                              maxLines: 3,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.left,
                            ),
                      ),
                    ),
                  ],
                ),
                const Expanded(
                  child: SizedBox(),
                ),
                Padding(
                  padding: paddingButton?? const EdgeInsets.all(0.0),
                  child: ButtonBanner(
                    decorator: buttonDecorator ??
                        const ButtonDecorator(text: "text button"),
                    onTap: buttonClick,
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: paddingImages ?? const EdgeInsets.all(0.0),
          child: ImagesBanner(
            decorator: imageDecorator ?? const ImageDecorator(),
          ),
        ),
      ],
    );
  }
}
