import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:news_banner/news_banner/decorators/image_decorator.dart';

class ImagesBanner extends StatelessWidget {
  final ImageDecorator decorator;

  const ImagesBanner({
    Key? key,
    required this.decorator,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (decorator.svgImages) {
      return Align(
        alignment: Alignment.centerRight,
        child: ClipRRect(
          borderRadius: decorator.clipRectRadius ?? BorderRadius.circular(50),
          child: decorator.pathToTheImages.isNotEmpty
              ? SvgPicture.asset(
                  decorator.pathToTheImages,
                  width: decorator.width ?? 110,
                  height: decorator.height ?? 110,
                )
              : const SizedBox(),
        ),
      );
    }
    if (decorator.widget == null) {
      return Align(
        alignment: Alignment.centerRight,
        child: decorator.pathToTheImages.isEmpty
            ? ClipRRect(
                borderRadius:
                    decorator.clipRectRadius ?? BorderRadius.circular(50),
                child: CachedNetworkImage(
                  imageUrl: decorator.urlImages,
                  errorWidget: (context, url, error) {
                    return decorator.errorLoadImages ??
                        Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: Image.asset("lib/resources/assets/icons/error_images.png"),
                        );
                  },
                  placeholder: (context, url) => CircularProgressIndicator(
                    color: decorator.colorPlaceholder,
                  ),
                  width: decorator.width ?? 110,
                  height: decorator.height ?? 110,
                ),
              )
            : Image.asset(
                decorator.pathToTheImages,
              ),
      );
    } else {
      return Align(
        alignment: Alignment.centerRight,
        child: decorator.widget,
      );
    }
  }
}
