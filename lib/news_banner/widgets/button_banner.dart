import 'package:flutter/material.dart';
import 'package:news_banner/news_banner/decorators/button_decorator.dart';

class ButtonBanner extends StatelessWidget {
  final ButtonDecorator decorator;
  final ValueChanged<bool>? onTap;

  const ButtonBanner({
    Key? key,
    required this.decorator,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (onTap != null) {
          onTap!(true);
        }
      },
      borderRadius: decorator.borderRadiusElevation ?? BorderRadius.circular(5),
      child: Container(
            constraints: decorator.constraints ??
                const BoxConstraints(
                  minHeight: 30,
                  minWidth: 140,
                ),
            decoration: decorator.decorationButton ??
                BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
            child: decorator.widget ?? Padding(
              padding: const EdgeInsets.only(left: 28,right: 28,top: 6),
              child: Text(
                decorator.text,
                textAlign: TextAlign.center,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: decorator.textStyle ??
                    const TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
              ),
            ),
          ),
    );
  }
}
