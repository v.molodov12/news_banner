import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:news_banner/news_banner/decorators/carousel_decorator.dart';
import 'package:news_banner/news_banner/news_item.dart';

class CarouselNewsBanner extends StatelessWidget {
  ///[items]List of typed cards to display.
  ///[items]Список типизированных карточек для показа.
  final List<NewsItem>? items;
  ///[customItems]List of any cards to display, is a higher priority than[items].
  ///[customItems]Список любых карточек для показа,является более приоритетным чем[items].
  final List<Widget>? customItems;
  ///[decorator]A class for customizing the appearance of the widget.
  ///[decorator]Класс для настройки внешнего вида виджета.
  final CarouselDecorator decorator;

  const CarouselNewsBanner({
    Key? key,
    this.customItems,
    this.items,
    required this.decorator,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: decorator.backgroundDecoration ??
          const BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [Color(0XFF005CB1), Color(0XFFF9DFC1)],
            ),
          ),
      child: Padding(
        padding: const EdgeInsets.all(
          1.2,
        ),
        child: Container(
          padding: const EdgeInsets.all(12),
          decoration: decorator.boxDecoration ??
              const BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(20),
                ),
                gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [Color(0XFF005CB1), Color(0XFFF9DFC1)],
                ),
              ),
          child: CarouselSlider(
            carouselController:
                decorator.carouselController ?? CarouselController(),
            options: CarouselOptions(
              height: decorator.height,
              viewportFraction: 1.0,
              enlargeCenterPage: false,
              autoPlayInterval: decorator.autoPlayInterval,
              autoPlayAnimationDuration: const Duration(milliseconds: 800),
              autoPlay: decorator.autoPlay,
            ),
            items: customItems ?? (items ?? []),
          ),
        ),
      ),
    );
  }
}
