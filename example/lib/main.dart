import 'package:flutter/material.dart';
import 'package:news_banner/news_banner/carousel_news_banner.dart';
import 'package:news_banner/news_banner/decorators/carousel_decorator.dart';
import 'package:news_banner/news_banner/decorators/image_decorator.dart';
import 'package:news_banner/news_banner/news_item.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo news_banner'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 140,
              width: 360,
              child: CarouselNewsBanner(
                items: [
                  NewsItem(
                    imageDecorator: const ImageDecorator(
                      urlImages: "c858524/v858524796/d3be5/-v5i6Vk5qvA.jpg?size=0x0&quality=90&proxy=1&sign=71b1e8655d8d22725bffd135febe91a2&c_uniq_tag=I_lMw4IgqHUUrm8vZE46Ok34_to8tvyLmNMyKbIhdv0&type=video_thumb",
                    ),
                    buttonClick: (value) {},
                  ),
                ],
                decorator: const CarouselDecorator(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
